package kz.aitu.midJava.service;

import kz.aitu.midJava.entity.Person;
import kz.aitu.midJava.repository.PersonRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class PersonService {

    private final PersonRepository personRepository;

    public List<Person> getAll(){
        return personRepository.findAll ();
    }

    public Person getById(Long id){
        return personRepository.findAllById ( id );
    }

    public Person  create (Person person){
        return personRepository.save ( person );
    }

    public Person  update (Person person){
        return personRepository.save ( person );
    }

    public void delete(Long id){
        personRepository.deleteById ( id );
    }

}
