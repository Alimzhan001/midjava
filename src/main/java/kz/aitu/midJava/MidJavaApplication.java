package kz.aitu.midJava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MidJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MidJavaApplication.class, args);
	}

}
