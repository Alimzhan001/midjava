package kz.aitu.midJava.controller;

import kz.aitu.midJava.entity.Person;
import kz.aitu.midJava.service.PersonService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
public class PersonController {

    private final PersonService personService;

    @GetMapping("/api/v2/users")
    public ResponseEntity<?> getPersons(){
        return ResponseEntity.ok ( personService.getAll () );
    }

    @GetMapping("/api/v2/users/{personId}")
    public ResponseEntity<?> getPerson(@PathVariable Long personId){
        return ResponseEntity.ok ( personService.getById ( personId ) );
    }

    @PostMapping("/api/v2/users")
    public ResponseEntity<?> savePerson(@RequestBody Person person){
        return ResponseEntity.ok ( personService.create ( person ) );
    }

    @PutMapping("/api/v2/users")
    public ResponseEntity<?> updatePerson(@RequestBody Person person){
        return ResponseEntity.ok ( personService.update ( person ) );
    }

    @DeleteMapping("/api/v2/users/{personId}")
    public void deletePerson(@PathVariable Long personId){
        personService.delete ( personId );
    }

}
