drop table if exists Person;

create table Person(
    id serial ,
    first_name varchar (255),
    last_name varchar (255),
    city varchar (255),
    phone varchar (255),
    telegram varchar (255)
);